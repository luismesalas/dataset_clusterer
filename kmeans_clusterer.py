import argparse
import csv
import importlib.machinery
import logging
import os
import urllib.request
import zipfile
from datetime import datetime

import types
from pyspark import SparkConf
from pyspark import SparkContext
from pyspark.mllib.clustering import KMeans


class KMeansDatasetClusterer:
    def __init__(self, settings_path):
        loader = importlib.machinery.SourceFileLoader(settings_path, settings_path)
        self.__settings = types.ModuleType(loader.name)
        self.__spark_conf = SparkConf().setMaster("local[1]").setAppName("Movielens modeler local app").set(
            'spark.executor.memory', '8G').set('spark.driver.memory', '12G')
        self.__sc = SparkContext(conf=self.__spark_conf)
        loader.exec_module(self.__settings)

    def run_kmeans_dataset_clusterer(self, output_path):

        try:
            logging.info("Running KMeans movie clusterer...")

            unzipped_files_path = self.get_raw_files(output_path)

            movies_file_path = os.path.join(unzipped_files_path, self.__settings.CSV_FILE_PATTERN.format(
                self.__settings.MOVIES_NAME))
            movies_with_genres_file_path = os.path.join(unzipped_files_path, self.__settings.CSV_FILE_PATTERN.format(
                self.__settings.MOVIES_GENRES_MATRIX_NAME))

            if not os.path.exists(movies_with_genres_file_path):
                self.generate_movies_with_genres_matrix_file(movies_file_path, movies_with_genres_file_path)

            # Load and parse the data
            parsedData = self.get_movies_rdd(movies_with_genres_file_path)

            # Build the model (cluster the data)
            clusters = KMeans.train(parsedData.map(lambda m: m[1]), k=15, seed=0)

            # Evaluate clustering by computing Within Set Sum of Squared Errors
            WSSSE = clusters.computeCost(parsedData.map(lambda m: m[1]))
            print("Within Set Sum of Squared Error = " + str(WSSSE))

            #Get cluster prediction for each movie
            prediction_by_movie = parsedData.map(lambda m: (m[0], clusters.predict(m[1]))).collectAsMap()

            #Loading ratings file
            ratings_file_path = os.path.join(unzipped_files_path, self.__settings.CSV_FILE_PATTERN.format(
                self.__settings.RATINGS_NAME))
            ratings_rdd = self.get_ratings_rdd(ratings_file_path)

            #Add clusterId to ratings
            ratings_with_cluster = ratings_rdd.map(lambda m: [m[0], m[1], m[2], prediction_by_movie.get(m[1])])

            print(ratings_with_cluster.take(20))




        except Exception as ex:
            logging.exception("Exception: {}".format(ex.args))
            exit(-1)

    def generate_k_with_wss(self, parsedData, stop, path):
        for number in range(1, stop):
            clusters_tmp = KMeans.train(parsedData.map(lambda m: m[1]), k=number, seed=0)
            WSSSE_tmp = clusters_tmp.computeCost(parsedData.map(lambda m: m[1]))
            print("k={}, wssse={}".format(number, WSSSE_tmp))

    def get_raw_files(self, output_path):
        if not os.path.exists(output_path):
            os.makedirs(output_path)
        # Parameters and some configuration checks
        if not self.get_movielens_dataset(output_path):
            raise Exception("Could not get movielens dataset zip file")
        unzipped_files_path = self.get_movielens_unzipped_files(output_path)
        return unzipped_files_path

    def get_movies_rdd(self, movies_file_path):
        movies_data = self.__sc.textFile(movies_file_path)
        movies_data_header = movies_data.take(1)[0]

        # Filter: ignoring header line, split fields and keep only movieId and name
        movies_data_rdd = movies_data.filter(lambda line: line != movies_data_header).map(
            lambda line: line.split(",")).map(lambda tokens: (int(tokens[0]), list(map(int, tokens[1:])))).cache()

        return movies_data_rdd

    def get_ratings_rdd(self, ratigs_file_path):
        ratings_data = self.__sc.textFile(ratigs_file_path)
        ratings_data_header = ratings_data.take(1)[0]

        # Filter: ignoring header line, split fields and keep only userId, movieId and rating
        ratings_data_rdd = ratings_data.filter(
            lambda line: line != ratings_data_header).map(lambda line: line.split(",")).map(
            lambda tokens: (int(tokens[0]), int(tokens[1]), float(tokens[2]))).cache()

        return ratings_data_rdd

    def generate_movies_with_genres_matrix_file(self, movies_file_path, movies_with_genres_file_path):
        movies_file = open(movies_file_path, 'r')
        movies_file_reader = csv.DictReader(movies_file)
        with open(movies_with_genres_file_path, 'w') as movies_out:
            wr = csv.writer(movies_out, quoting=csv.QUOTE_NONNUMERIC)

            header_row = []
            header_row.append('movieId')
            for category in self.__settings.KNOWN_CATEGORIES:
                header_row.append(category)

            wr.writerow(header_row)

            for movie in movies_file_reader:
                movie_vector = [0] * (len(self.__settings.KNOWN_CATEGORIES) + 1)
                movie_vector[0] = int(movie['movieId'])
                # genres field in movies items is a pipe-separated field, we will transform it to an array
                movie['genres'] = movie['genres'].split('|')
                for genre in movie['genres']:
                    if genre != "(no genres listed)":
                        index = self.__settings.KNOWN_CATEGORIES.index(genre) + 1
                        movie_vector[index] = 1

                wr.writerow(movie_vector)

    # Check if ml-latest.zip file exists on path and try to download it if it doesn't exists
    def get_movielens_dataset(self, path):
        if not os.path.exists(os.path.join(path, self.__settings.MOVIELENS_ZIPFILE_NAME)):
            # File does not exist, it tries to download it
            logging.info("Dataset zip file not found in '{}'. Downloading from: '{}'".format(path,
                                                                                             self.__settings.MOVIELENS_DOWNLOAD_URL))
            urllib.request.urlretrieve(self.__settings.MOVIELENS_DOWNLOAD_URL,
                                       os.path.join(path, self.__settings.MOVIELENS_ZIPFILE_NAME))
            # Returning success if file could be downloaded to path
            return os.path.exists(os.path.join(path, self.__settings.MOVIELENS_ZIPFILE_NAME))
        # File already exists
        logging.info("Dataset zip file already exists")
        return True

    # Check if all csv files of ml-latest.zip exists in output_path. If one or more files missing, it tries to unzip ml-latest.zip file
    def get_movielens_unzipped_files(self, output_path):
        unzipped_files_path = os.path.join(output_path, self.__settings.MOVIELENS_FILES_FOLDER)

        if not self.files_required_exists(unzipped_files_path):
            logging.info("Unzipping dataset zip file in '{}'".format(unzipped_files_path))
            # One or more files missing, unzipping ml-latest.zip file
            zip_ref = zipfile.ZipFile(os.path.join(output_path, self.__settings.MOVIELENS_ZIPFILE_NAME), 'r')
            zip_ref.extractall(output_path)
            zip_ref.close()

        if not self.files_required_exists(unzipped_files_path):
            # File couldn't be unzipped, exception raised
            raise Exception("Could not unzip movielens dataset file")
        logging.info("CSV files placed in '{}'".format(unzipped_files_path))
        return unzipped_files_path

    # Check if all csv files contained in ml-latest.zip exists on that path
    def files_required_exists(self, path):
        for file in self.__settings.MOVIELENS_REQUIRED_FILES:
            if not os.path.exists(os.path.join(path, file)):
                return False
        return True


def main():
    loader = importlib.machinery.SourceFileLoader("config/settings.py", "config/settings.py")
    settings = types.ModuleType(loader.name)
    loader.exec_module(settings)
    logging.basicConfig(filename=settings.LOG_PATH + str(datetime.today()), level=logging.INFO,
                        format=settings.LOG_FORMAT)

    parser = argparse.ArgumentParser(
        description='Movielens modeler. It uses Alternating Least Squares matrix factorization for creating a prediction model.')
    parser.add_argument('--output_path',
                        help='Path for storing staging and output files. [User home]/movielens_modeler_output by default',
                        required=False,
                        default=os.path.join(os.path.expanduser("~"), settings.OUTPUT_FOLDER_DEFAULT_NAME))

    args = parser.parse_args()
    logging.info("Params <{}>".format(args))
    logging.info("Starting movies flatter")
    try:
        kmeans_dataset_clusterer = KMeansDatasetClusterer("config/settings.py")
        kmeans_dataset_clusterer.run_kmeans_dataset_clusterer(args.output_path)
    except Exception as ex_main:
        logging.error("ERROR: An error raised and the process ended: {}".format(str(ex_main)))
        exit(-1)

    logging.info("KMeans Cluster process finished!")


if __name__ == '__main__':
    main()
