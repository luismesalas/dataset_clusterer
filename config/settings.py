LOG_PATH = "/tmp/"
LOG_FORMAT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
CSV_FILE_PATTERN = "{}.csv"
GENOME_SCORES_NAME = "genome-scores"
GENOME_TAGS_NAME = "genome-tags"
LINKS_NAME = "links"
MOVIES_NAME = "movies"
MOVIES_GENRES_MATRIX_NAME = "movies_with_genres_matrix"
RATINGS_NAME = "ratings"
TAGS_NAME = "tags"
OUTPUT_FOLDER_DEFAULT_NAME = "movielens_classifier_output"
MOVIELENS_ZIPFILE_NAME = "ml-latest.zip"
MOVIELENS_DOWNLOAD_URL = "http://files.grouplens.org/datasets/movielens/ml-latest.zip"
MOVIELENS_FILES_FOLDER = "ml-latest"
MOVIELENS_REQUIRED_FILES = [CSV_FILE_PATTERN.format(GENOME_SCORES_NAME),
                            CSV_FILE_PATTERN.format(GENOME_TAGS_NAME),
                            CSV_FILE_PATTERN.format(LINKS_NAME),
                            CSV_FILE_PATTERN.format(MOVIES_NAME),
                            CSV_FILE_PATTERN.format(RATINGS_NAME),
                            CSV_FILE_PATTERN.format(TAGS_NAME)]
KNOWN_CATEGORIES = [
    'Action', 'Adventure', 'Animation', 'Children', 'Comedy', 'Crime', 'Documentary', 'Drama', 'Fantasy', 'Film-Noir',
    'Horror', 'IMAX', 'Musical', 'Mystery', 'Romance', 'Sci-Fi', 'Thriller', 'War', 'Western']
KMEANS_MODEL_FOLDER= "kmeans-model"
KMEANS_PREDICTED_MOVIES_FOLDER= "kmeans-predicted-movies"